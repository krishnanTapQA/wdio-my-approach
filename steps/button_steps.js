/**
 * I click on button "button selector"
 * I click on button "button name"
 */

var cucumber = require("cucumber")


cucumber.When(/^I click the button "(.*?)"$/, element => {
    checkPageContext();
    let PageObject = require(`../page_model/${global.pageContext}.json`)
    browser.element(PageObject.buttons[element]).click
    browser.element(PageObject.buttons[element]).setValue("test123")
});