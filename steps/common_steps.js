var cucumber = require("cucumber")
/**
 * all methods will use this context to decide which page we are on
 */
global.pageContext=""
global.checkPageContext=function (){
    if(pageContext === ""){
        return "Set Page Context with -->Given I am on page 'Some Page' and fail test"
    }
}
/**
 * Common Steps
 */

cucumber.Given(/^I am on the "(.*?)" page$/, pageName => {
  browser.url("https://google.com")
  global.pageContext = pageName
});
